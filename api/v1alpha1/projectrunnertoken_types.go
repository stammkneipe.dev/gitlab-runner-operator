/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// ProjectRunnerTokenSpec defines the desired state of ProjectRunnerToken
type ProjectRunnerTokenSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// The name of the Issuer to use for the Gitlab API Calls
	Issuer string `json:"issuer,omitempty"`

	// Is the Issuer a cluster issuer
	// Defaults to false
	ClusterIssuer bool `json:"clusterIssuer,omitempty"`

	// The ID of the Project to generate Gitlab Runner Secrets for
	ProjectId int `json:"projectId,omitempty"`

	// A comma separated list of tags
	Tags string `json:"tags,omitempty"`

	// Description for the Runner
	Description string `json:"description,omitempty"`
}

// ProjectRunnerTokenStatus defines the observed state of ProjectRunnerToken
type ProjectRunnerTokenStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// Conditions store the status conditions of the Token
	Conditions []metav1.Condition `json:"conditions,omitempty" patchStrategy:"merge" patchMergeKey:"type" protobuf:"bytes,1,rep,name=conditions"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// ProjectRunnerToken is the Schema for the projectrunnertokens API
// +kubebuilder:printcolumn:name="Issuer",type="string",JSONPath=".spec.issuer",description="The name of the Credential Provider to use for the Gitlab API Calls"
// +kubebuilder:printcolumn:name="Cluster Issuer",type="boolean",JSONPath=".spec.clusterIssuer",description="Is the Credential Provider a cluster provider"
// +kubebuilder:printcolumn:name="Project ID",type="number",JSONPath=".spec.projectId",description="The ID of the Project to generate Gitlab Runner Secrets for"
// +kubebuilder:printcolumn:name="Status",type="string",JSONPath=".status.conditions[?(@.type==\"Available\")].reason",description="The status of this resource"
type ProjectRunnerToken struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ProjectRunnerTokenSpec   `json:"spec,omitempty"`
	Status ProjectRunnerTokenStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// ProjectRunnerTokenList contains a list of ProjectRunnerToken
type ProjectRunnerTokenList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ProjectRunnerToken `json:"items"`
}

func init() {
	SchemeBuilder.Register(&ProjectRunnerToken{}, &ProjectRunnerTokenList{})
}
