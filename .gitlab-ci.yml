### Extend Pipeline
variables:
  APKO_FILE: "apko.yml"
  MELANGE_FILE: "melange.yml"
  RUNNER: "kubernetes"

stages:
  - build
  - containerize
  - scan
  - release

include:
  - project: 'stammkneipe-dev/gitlab-ci-templates'
    ref: main
    file: '.apko.yml'
  - project: 'stammkneipe-dev/gitlab-ci-templates'
    ref: main
    file: '.release.yml'

### Helm Templates
.bundle:
  stage: build
  image: registry.gitlab.com/stammkneipe-dev/operator-packager:latest
  script:
    - make bundle
  artifacts:
    paths:
      - bundle

.upload:
  stage: containerize
  variables:
    HELM_EXPERIMENTAL_OCI: 1
  image: registry.gitlab.com/stammkneipe-dev/operator-packager:latest
  before_script:
    - git config --global --add safe.directory '*'
    - VERSION=$(git describe --tags `git rev-list --tags --max-count=1` 2>/dev/null)
    - if [ -z "$VERSION" ]; then VERSION="0.1.0"; fi
    - if [ -z "$CI_COMMIT_TAG" ]; then VERSION="${VERSION}-${CI_PIPELINE_IID}"; fi
  script:
    - kustomize build config/default | helmify ${CI_PROJECT_NAME}
    - yq e -P ".version = \"${VERSION}\"" -i ${CI_PROJECT_NAME}/Chart.yaml
    - yq e -P ".appVersion = \"${VERSION}\"" -i ${CI_PROJECT_NAME}/Chart.yaml
    - helm package ${CI_PROJECT_NAME} --destination ./public
    - helm repo add --username gitlab-ci-token --password ${CI_JOB_TOKEN} ${CI_PROJECT_NAME} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/stable
    - helm plugin install https://github.com/chartmuseum/helm-push
    - helm cm-push ./public/${CI_PROJECT_NAME}-${VERSION}.tgz ${CI_PROJECT_NAME}
    - helm repo index --url https://${CI_PROJECT_NAMESPACE}.gitlab.io/${CI_PROJECT_NAME} .
    - mv index.yaml ./public
  artifacts:
    paths:
      - gitlab-runner-operator
      - gitlab-runner-operator-*
      - public

### Features Branches
bundle_feature:
  extends:
    - .bundle
    - .feature_renovate

upload_feature:
  extends:
    - .upload
    - .feature_renovate

build_feature:
  before_script:
    - melange version
    - melange keygen
  extends:
    - .build_package
    - .feature_renovate
  artifacts:
    paths:
      - melange.rsa
      - melange.rsa.pub
      - ./packages

containerize_feature:
  before_script:
    - apko version
    - mkdir -p sbom
  extends:
    - .containerize_package
    - .feature_renovate

scan-feature:
  extends:
  - .scan-container
  - .feature_renovate

sbom-feature:
  extends:
  - .scan-sbom
  - .feature_renovate

### MAIN Branch
bundle_latest:
  extends:
    - .bundle
    - .latest

upload_latest:
  extends:
    - .upload
    - .latest

build_latest:
  extends:
    - .build_package
    - .latest

containerize_latest:
  extends:
    - .containerize_package
    - .latest

scan-latest:
  extends:
  - .scan-container
  - .latest

sbom-latest:
  extends:
  - .scan-sbom
  - .latest

### Version Tag
bundle_tag:
  extends:
    - .bundle
    - .tag

pages:
  extends:
    - .upload
    - .tag

build_tag:
  extends:
    - .build_package
    - .tag

containerize_tag:
  variables:
    FULL_IMAGE_NAME: $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  extends:
    - .containerize_package
    - .tag

scan-tag:
  extends:
  - .scan-container
  - .tag

sbom-tag:
  extends:
  - .scan-sbom
  - .tag
