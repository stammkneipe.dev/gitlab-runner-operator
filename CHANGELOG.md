# [1.3.0](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/compare/1.2.0...1.3.0) (2024-08-08)


### Features

* upgrade operator-sdk ([aa266a4](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/aa266a4cbc165ecd9064623b24bd963caa595d69))

# [1.2.0](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/compare/1.1.0...1.2.0) (2024-08-04)


### Bug Fixes

* go mod ([cff7b2e](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/cff7b2e06328a0409b9da6c7c28f68b0b6d29c96))
* operator-sdk upgrade ([ddd8e95](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/ddd8e955cde866e963fd9187333999ea64a615b2))
* package ([302ff36](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/302ff3689a812b5f6b05fd6fd378ddb6039c15a9))
* toolchain ([a90d016](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/a90d016cf6513cfb61af6ab6deaa220de49b1621))
* toolchain ([9cd9e0e](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/9cd9e0e0ce6ac69853cb8dfe401d1223571e6446))


### Features

* Upgrade Operator SDK ([edb6a10](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/edb6a10419586f2f5a14167c56b7a354c74a76e7))

# [1.1.0](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/compare/1.0.10...1.1.0) (2024-06-16)


### Features

* dynamic update cycle ([b72b065](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/b72b065818bcb5c6f95e132a42deb390494366c0))

## [1.0.10](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/compare/1.0.9...1.0.10) (2024-04-02)


### Bug Fixes

* new and old name needed ([48e0af7](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/48e0af7ee0827d5501fbb599ef07a6b41d6461b8))

## [1.0.9](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/compare/1.0.8...1.0.9) (2024-03-28)


### Bug Fixes

* secret name ([2521fdb](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/2521fdb3670301332f1cb72b78c9678ab6e5d446))

## [1.0.8](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/compare/1.0.7...1.0.8) (2024-03-17)


### Bug Fixes

* naming ([0b20888](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/0b20888194e2b9b5629cf410ea241584404c55a1))

## [1.0.6](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/compare/1.0.5...1.0.6) (2024-02-11)


### Bug Fixes

* rbac secret ([1f9cb54](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/1f9cb546ab55e86b737b9e3160c7c0ae9c88c64b))

## [1.0.5](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/compare/1.0.4...1.0.5) (2024-02-11)


### Bug Fixes

* rbac ([32b0646](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/32b0646c0bef0179eb50afd2a884b6ab2d725446))

## [1.0.4](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/compare/1.0.3...1.0.4) (2024-02-04)


### Bug Fixes

* container startup ([4c7a67c](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/4c7a67cba7e041793c806e2943a06e436ab4e1a7))
* runner ([260f5fe](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/260f5fecc5c044f6d44188adfdf32fa090270943))


### Reverts

* arm64 ([0e20d4b](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/0e20d4b6dbd9deb66cd1c5a5401ceac373a0d429))

## [1.0.3](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/compare/1.0.2...1.0.3) (2023-12-27)


### Bug Fixes

* Add unit test for LabelsForToken function ([d19b008](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/d19b0089d629e0461d9efe5916d68a2ec6387c6a))

## [1.0.2](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/compare/1.0.1...1.0.2) (2023-12-27)


### Bug Fixes

* GitLab CI configuration and add melange Kubernetes manifest ([4ba5f11](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/4ba5f117f05be8527d9be2b51ac2e01369dfed63))
* Update version in Chart.yaml and use dynamic version in helm package ([0a4c284](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/0a4c2846dbcc9264a42da857a0ae48ed4a525420))

## [1.0.1](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/compare/1.0.0...1.0.1) (2023-12-21)


### Bug Fixes

* release ([fc84aa0](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/fc84aa0d2b45f89a57544f974ab2c63bff7aa21d))

# 1.0.0 (2023-12-20)


### Bug Fixes

* chart name ([13dac0e](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/13dac0ee61a4d86f04a9b75b1dca31e821187029))
* cm push ([9f8c689](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/9f8c689ccc0f606dab4da16f9c3855b7055a717d))
* dockerfile ([6e79207](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/6e79207344ddb95dda9e174cdfe41edef6a9d5aa))
* helm chart ([7ed4bb8](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/7ed4bb8896746ab5887dc0961031575952dcd8e7))
* ids ([cdb64b8](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/cdb64b8d6fb4bbc4acbfbcad53ef0f06eb1ab48a))


### Features

* add helm chart ([edb9df0](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/edb9df0fab98be344cdee780e021cd3af3b4e68f))
* add melange and apko support [ci skip] ([68be250](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/68be250048bdd33b99e7d5781f24040a0faf3e84))
* crds and api ([6906c88](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/6906c8831f91ce2c87b22e414a2e8a52502d90a1))
* helm chart [ci-skip] ([1d8cf02](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/1d8cf022b2e80c50dc892e985dd1f438bdce3c3f))
* initial structure ([337a462](https://gitlab.com/stammkneipe-dev/gitlab-runner-operator/commit/337a46230a3a9340bda62876e1c5e1c84f5b21f2))
