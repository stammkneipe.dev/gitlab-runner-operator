/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"time"

	"github.com/xanzy/go-gitlab"
	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	gitlabv1alpha1 "gitlab.com/stammkneipe-dev/gitlab-runner-operator/api/v1alpha1"
)

const (
	IssuerTypeState = "Issuer"
)

var (
	NamespacedGitlabClient map[string]map[string]*gitlab.Client
)

// IssuerReconciler reconciles a Issuer object
type IssuerReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=gitlab.gitlab-runner.domnick.dev,resources=issuers,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=gitlab.gitlab-runner.domnick.dev,resources=issuers/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=gitlab.gitlab-runner.domnick.dev,resources=issuers/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Issuer object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.1/pkg/reconcile
func (r *IssuerReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	_ = log.FromContext(ctx)

	log.Log.Info("Processing Issuer", "Namespace", req.NamespacedName, "Name", req.Name)

	// Get Issuer CRD
	issuer := &gitlabv1alpha1.Issuer{}
	if err := r.Get(ctx, req.NamespacedName, issuer); err != nil {
		if apierrors.IsNotFound(err) {
			log.Log.Info("Issuer not found. Ignoring since object must be deleted.")
			return ctrl.Result{}, nil
		}
		log.Log.Error(err, "Failed to get Issuer.")
	}

	// Initialize Gitlab Map
	_, ok := NamespacedGitlabClient[req.Namespace]
	if !ok {
		NamespacedGitlabClient[req.Namespace] = make(map[string]*gitlab.Client)
	}

	// Start the Reconciliation
	conditions := &issuer.Status.Conditions
	if len(*conditions) == 0 {
		meta.SetStatusCondition(conditions, metav1.Condition{
			Type:    IssuerTypeState,
			Status:  metav1.ConditionUnknown,
			Reason:  "Initializing",
			Message: "Starting reconciliation",
		})
		log.Log.Info("Condition", "Length", len(issuer.Status.Conditions))
		if err := r.Status().Update(ctx, issuer); err != nil {
			log.Log.Error(err, "Failed to update Issuer status")
			return ctrl.Result{}, err
		}
		// Start the next step
		return ctrl.Result{}, nil
	}

	// Check the Timestamps against the Secret
	currentCondition := (*conditions)[0]

	// Get the Secret
	accessToken := &corev1.Secret{}
	if err := r.Get(ctx, types.NamespacedName{Name: issuer.Spec.AccessTokenSecret, Namespace: issuer.Namespace}, accessToken); err != nil {
		log.Log.Error(err, "Failed to get the Gitlab Access Token Secret", "Namespace", accessToken.Namespace, "Name", accessToken.Name)
		return ctrl.Result{RequeueAfter: time.Minute * 1}, err
	}

	// Restart Reconciliation when the Secret is Updated
	if currentCondition.LastTransitionTime.Before(accessToken.ObjectMeta.ManagedFields[len(accessToken.ObjectMeta.ManagedFields)-1].Time) {
		log.Log.Info("Secret has been updated! Refreshing CRD")
		meta.SetStatusCondition(conditions, metav1.Condition{
			Type:    ClusterIssuerTypeState,
			Status:  metav1.ConditionUnknown,
			Reason:  "Initializing",
			Message: "Re-Starting reconciliation",
		})
		if err := r.Status().Update(ctx, issuer); err != nil {
			log.Log.Error(err, "Failed to update Issuer status")
			return ctrl.Result{}, err
		}
		// Start the next step
		return ctrl.Result{}, nil
	}

	// Act depending on the Condition
	currentConditionStatus := currentCondition.Reason
	switch currentConditionStatus {
	case "Initializing":
		// Check if the Access Key is Valid
		gitlabToken := string(accessToken.Data[issuer.Spec.AccessTokenKey])
		git, err := gitlab.NewClient(gitlabToken, gitlab.WithBaseURL(issuer.Spec.GitlabApiUrl))
		if err != nil {
			log.Log.Error(err, "Failed to initialize Gitlab API")
			return ctrl.Result{}, err
		}
		metadata, response, err := git.Metadata.GetMetadata()
		if err != nil {
			log.Log.Error(err, "Failed to use Gitlab API")
			// Initialize the Gitlab API Client
			meta.SetStatusCondition(conditions, metav1.Condition{
				Type:    IssuerTypeState,
				Status:  metav1.ConditionTrue,
				Reason:  "Unavailable",
				Message: fmt.Sprintf("Failed to use Gitlab API: %s", err),
			})
			if err := r.Status().Update(ctx, issuer); err != nil {
				log.Log.Error(err, "Failed to update Issuer status")
				return ctrl.Result{RequeueAfter: time.Minute * 1}, err
			}
			return ctrl.Result{}, err
		}
		log.Log.Info(response.Status, "Version", metadata.Version)
		NamespacedGitlabClient[issuer.Namespace][issuer.Name] = git
		// Initialize the Gitlab API Client
		meta.SetStatusCondition(conditions, metav1.Condition{
			Type:    IssuerTypeState,
			Status:  metav1.ConditionTrue,
			Reason:  "Available",
			Message: "Issuer successfully initialized",
		})
		// Updating the state will force the reconciler to begin working again
		if err := r.Status().Update(ctx, issuer); err != nil {
			log.Log.Error(err, "Failed to update the status")
			return ctrl.Result{}, err
		}
	case "Unavailable":
		// Retry depending on the error
		meta.SetStatusCondition(conditions, metav1.Condition{
			Type:    GroupRunnerTokenTypeState,
			Status:  metav1.ConditionUnknown,
			Reason:  "Initializing",
			Message: "Starting Initializing",
		})
		if err := r.Status().Update(ctx, issuer); err != nil {
			log.Log.Error(err, "Failed to update Issuer status")
			return ctrl.Result{}, err
		}
		return ctrl.Result{
			RequeueAfter: time.Hour * 1,
		}, nil
	case "Available":
		// Check if the Gitlab Client has been initialized
		_, ok := (NamespacedGitlabClient[issuer.Namespace][issuer.Name])
		if !ok {
			// Re-Initialize the Client
			log.Log.Info("Re-Initializing Issuer")
			meta.SetStatusCondition(conditions, metav1.Condition{
				Type:    IssuerTypeState,
				Status:  metav1.ConditionUnknown,
				Reason:  "Initializing",
				Message: "Starting reconciliation",
			})
			if err := r.Status().Update(ctx, issuer); err != nil {
				log.Log.Error(err, "Failed to update Issuer status")
				return ctrl.Result{}, err
			}
		}
	default:
		// Set State if State was unknown
		meta.SetStatusCondition(conditions, metav1.Condition{
			Type:    IssuerTypeState,
			Status:  metav1.ConditionUnknown,
			Reason:  "Initializing",
			Message: "Starting reconciliation",
		})
		if err := r.Status().Update(ctx, issuer); err != nil {
			log.Log.Error(err, "Failed to update Issuer status")
			return ctrl.Result{}, err
		}
	}
	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *IssuerReconciler) SetupWithManager(mgr ctrl.Manager) error {
	NamespacedGitlabClient = make(map[string]map[string]*gitlab.Client)
	return ctrl.NewControllerManagedBy(mgr).
		For(&gitlabv1alpha1.Issuer{}).
		Complete(r)
}
