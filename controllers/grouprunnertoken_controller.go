/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"strings"
	"time"

	"github.com/xanzy/go-gitlab"
	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	gitlabv1alpha1 "gitlab.com/stammkneipe-dev/gitlab-runner-operator/api/v1alpha1"
	"gitlab.com/stammkneipe-dev/gitlab-runner-operator/utils"
)

const (
	GroupRunnerTokenTypeState = "Available"
)

// GroupRunnerTokenReconciler reconciles a GroupRunnerToken object
type GroupRunnerTokenReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=gitlab.gitlab-runner.domnick.dev,resources=grouprunnertokens,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=gitlab.gitlab-runner.domnick.dev,resources=grouprunnertokens/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=gitlab.gitlab-runner.domnick.dev,resources=grouprunnertokens/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the GroupRunnerToken object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.1/pkg/reconcile
func (r *GroupRunnerTokenReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	_ = log.FromContext(ctx)

	log.Log.Info("Processing Project Token", "Namespace", req.Namespace, "Name", req.Name)

	// Get Token CRD
	token := &gitlabv1alpha1.GroupRunnerToken{}
	if err := r.Get(ctx, req.NamespacedName, token); err != nil {
		if apierrors.IsNotFound(err) {
			log.Log.Info("Token not found. Ignoring since object must be deleted.")
			return ctrl.Result{}, nil
		}
		log.Log.Error(err, "Failed to get Token.")
	}

	// Start the Reconciliation
	conditions := &token.Status.Conditions
	if len(*conditions) == 0 {
		meta.SetStatusCondition(conditions, metav1.Condition{
			Type:    GroupRunnerTokenTypeState,
			Status:  metav1.ConditionUnknown,
			Reason:  "Initializing",
			Message: "Starting reconciliation",
		})
		if err := r.Status().Update(ctx, token); err != nil {
			log.Log.Error(err, "Failed to update Group Runner status")
			return ctrl.Result{}, err
		}
		// Start the next step
		return ctrl.Result{}, nil
	}

	// Check the Timestamps against the Secret
	currentCondition := (*conditions)[0]

	// Restart Reconciliation when the CRD is Updated
	if currentCondition.LastTransitionTime.Before(token.ObjectMeta.ManagedFields[len(token.ObjectMeta.ManagedFields)-1].Time) {
		log.Log.Info("CRD has been updated! Refreshing Secret")
		meta.SetStatusCondition(&token.Status.Conditions, metav1.Condition{
			Type:    GroupRunnerTokenTypeState,
			Status:  metav1.ConditionUnknown,
			Reason:  "Initializing",
			Message: "Starting reconciliation",
		})
		if err := r.Status().Update(ctx, token); err != nil {
			log.Log.Error(err, "Failed to update Group Runner status")
			return ctrl.Result{}, err
		}
		// Start the next step
		return ctrl.Result{}, nil
	}

	// Act depending on the Condition
	currentConditionStatus := currentCondition.Reason
	switch currentConditionStatus {
	case "Initializing":
		var gitlabClient *gitlab.Client
		var gitlabInitialized bool
		// Cluster or Namespace Issuer?
		if token.Spec.ClusterIssuer {
			gitlabClient, gitlabInitialized = ClusterGitlabClient[token.Spec.Issuer]
		} else {
			gitlabClient, gitlabInitialized = NamespacedGitlabClient[token.Namespace][token.Spec.Issuer]
		}
		// Check if the Gitlab Client is initialized
		if !gitlabInitialized {
			log.Log.Error(nil, "Could not get Gitlab Client", "Client", token.Spec.Issuer)
			return ctrl.Result{RequeueAfter: 1 * time.Minute}, nil
		}
		// Create Group Information
		runnerTags := strings.Split(token.Spec.Tags, ",")
		runner, response, err := gitlabClient.Users.CreateUserRunner(&gitlab.CreateUserRunnerOptions{
			RunnerType:  gitlab.String("group_type"),
			Description: &token.Spec.Description,
			GroupID:     &token.Spec.GroupId,
			TagList:     &runnerTags,
		})
		if err != nil {
			log.Log.Error(err, "Failed to create Group Runner", "Response", response.Status, "GroupID", token.Spec.GroupId)
			// TODO: Update Status
			return ctrl.Result{}, err
		}
		log.Log.Info("Successfully created Group Runner", "ID", runner.ID)
		// Check Secret
		secret := &corev1.Secret{}
		err = r.Get(ctx, types.NamespacedName{Name: token.Name, Namespace: token.Namespace}, secret)
		if err != nil {
			if apierrors.IsNotFound(err) {
				// No Secret exists and we create one
				ls := utils.LabelsForToken(token.Name)
				secretToken := &corev1.Secret{
					ObjectMeta: metav1.ObjectMeta{
						Name:      token.Name,
						Namespace: token.Namespace,
						Labels:    ls,
					},
					Type: corev1.SecretTypeOpaque,
					StringData: map[string]string{
						"runner-token":              runner.Token,
						"runner-registration-token": runner.Token,
					},
				}
				if err := r.Create(ctx, secretToken); err != nil {
					log.Log.Error(err, "Failed to create a new  Secret", "Namespace", secretToken.Namespace, "Name", secretToken.Name)
					return ctrl.Result{}, err
				}
			} else {
				// Some unknown error occurred
				log.Log.Error(err, "Failed to get Secret", "Namespace", token.Namespace, "Name", token.Name)
				return ctrl.Result{}, err
			}
		} else {
			// Update the Secret
			secret.StringData = map[string]string{
				"runner-token":              runner.Token,
				"runner-registration-token": runner.Token,
			}
			if err := r.Update(ctx, secret); err != nil {
				log.Log.Error(err, "Failed to update the Secret", "Namespace", secret.Namespace, "Name", secret.Name)
				return ctrl.Result{}, err
			}
		}
		// Update the status
		meta.SetStatusCondition(&token.Status.Conditions, metav1.Condition{
			Type:    GroupRunnerTokenTypeState,
			Status:  metav1.ConditionTrue,
			Reason:  "Available",
			Message: "Token created and up to date",
		})
		err = r.Status().Update(ctx, token)
		if err != nil {
			log.Log.Error(err, "Failed to update status")
			return ctrl.Result{}, err
		}
	case "Unavailable":
		// Retry depending on the error
		meta.SetStatusCondition(conditions, metav1.Condition{
			Type:    GroupRunnerTokenTypeState,
			Status:  metav1.ConditionUnknown,
			Reason:  "Initializing",
			Message: "Starting Initializing",
		})
		if err := r.Status().Update(ctx, token); err != nil {
			log.Log.Error(err, "Failed to update Token status")
			return ctrl.Result{}, err
		}
		return ctrl.Result{
			RequeueAfter: time.Hour * 1,
		}, nil
	case "Available":
		// TODO: Check if the Lifecycle is reached
	default:
		// Set State if State was unknown
		meta.SetStatusCondition(conditions, metav1.Condition{
			Type:    GroupRunnerTokenTypeState,
			Status:  metav1.ConditionUnknown,
			Reason:  "Initializing",
			Message: "Starting Initializing",
		})
		if err := r.Status().Update(ctx, token); err != nil {
			log.Log.Error(err, "Failed to update Token status")
			return ctrl.Result{}, err
		}
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *GroupRunnerTokenReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&gitlabv1alpha1.GroupRunnerToken{}).
		Complete(r)
}
