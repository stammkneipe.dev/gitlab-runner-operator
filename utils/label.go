package utils

// labelsForMemcached returns the labels for selecting the resources
// More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/common-labels/
func LabelsForToken(name string) map[string]string {
	return map[string]string{
		"app.kubernetes.io/name":       "Gitlab",
		"app.kubernetes.io/instance":   name,
		"app.kubernetes.io/part-of":    "gitlab-runner-operator",
		"app.kubernetes.io/created-by": "controller-manager",
	}
}
