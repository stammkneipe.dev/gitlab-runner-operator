package utils

import (
	"reflect"
	"testing"
)

func TestLabelsForToken(t *testing.T) {
	name := "test-instance"
	expected := map[string]string{
		"app.kubernetes.io/name":       "Gitlab",
		"app.kubernetes.io/instance":   name,
		"app.kubernetes.io/part-of":    "gitlab-runner-operator",
		"app.kubernetes.io/created-by": "controller-manager",
	}

	labels := LabelsForToken(name)

	if !reflect.DeepEqual(labels, expected) {
		t.Errorf("LabelsForToken() = %v, want %v", labels, expected)
	}
}
