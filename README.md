# Gitlab Runner Operator

> Create Tokens for Gitlab Runners to use

## Description

Gitlab Tokens are supposed to be secret and should not be stored in any repository.
However, generating, encrypting and transferring secrets to kubernetes can be a pain if you have a lot of projects and runners.
Simply create a [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with the permission to create runners and use this operator instead.

## Usage

### Helm

Install the Helm Chart

```sh
helm repo add runner-operator https://gitlab.com/api/v4/projects/51647243/packages/helm/stable
helm repo update
helm upgrade -i gitlab-runner runner-operator/gitlab-runner-operator
```

### Gitlab Secret

Create the [Personal Access Token](https://gitlab.com/-/user_settings/personal_access_tokens) [Secret](config/samples/gitlab_v1alpha1_secret.yaml) for the Issuer.

### Issuer

Create a [Issuer](config/samples/gitlab_v1alpha1_issuer.yaml) or [ClusterIssuer](config/samples/gitlab_v1alpha1_clusterissuer.yaml).

### Gitlab Token

Create a Gitlab Runner Token for your [Project](config/samples/gitlab_v1alpha1_projectrunnertoken.yaml) or [Group](config/samples/gitlab_v1alpha1_grouprunnertoken.yaml).

## Contributing

[Contributing](/CONTRIBUTING.md)

## License

[MIT License](/LICENSE)

## Authors and acknowledgment

- [Operator SDK](https://github.com/operator-framework/operator-sdk)
- [Patrick Domnick](https://gitlab.com/PatrickDomnick)
- [Henry Sachs](https://gitlab.com/DerAstronaut)
